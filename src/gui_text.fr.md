# Interface graphique pour un jeu de bataille spatialle 2D en temps réel

## Sujet

En 2015/2016, en tant que projet 2A, j'ai fait avec d'autres [un jeu de bataille spatialle 2D en temps réel](https://gitlab.com/RyDroid/PlanetWars2dRT-ideas) (en C99).
Il respecte le modèle MVC (Modèle, Vue, Controlleur).
[La partie "core"](https://gitlab.com/RyDroid/PlanetWars2dRT-core) (le modèle et le controlleur, c'est à dire le moteur du jeu) marche et est dans un dépôt git propre.
Depuis, j'ai rajouté de nombreux tests unitaires et corriger quelques bogues trouvés grâce aux tests, cette partie est donc à un stade mature.
[Une vue a été faite en SDL2](https://gitlab.com/RyDroid/PlanetWars2dRT-SDL2), mais elle se contente de démarrer une partie aléatoire puis de la jouer (l'utilisateur peut jouer avec des IA) et quitter en fin de partie.
C'est donc très sommaire.

Le but premier est de faire une interface graphique simple mais plus utilisable que l'actuelle : menu pour choisir une partie (carte déja faite, ou aléatoire, avec options pour le nombre de planètes et de joueurs), possibilité de rejouer après une fin de partie (au lieu de relancer le programme), choix graphique de la résolution, etc.
L'interface pourra reprendre l'existante ou être totalement nouvelle peu importe la technologie, aussi bien pour la partie graphique (Qt, GTK, wxWidgets, etc) que la partie langage (C, C++, Go, Rust, etc).
Il faudra bien sur utiliser la partie core, mais elle fait abstraction de nombres choses (il n'y a pas besoin de comprendre son fonctionnement interne pour une interface graphique).
En fonction de l'avancement, d'autres choses de votre opportunité pourront être faites : ajout de sons, enregistrement de partie, créateur graphique d'univers, internationnalisation, nouvelle(s) IA, ajout d'élément(s) de gameplay, paquet pour une distribution, effets graphiques, ou n'importe quoi d'autre autour du projet.

La partie core est sous [licence libre](https://www.gnu.org/licenses/license-list.fr.html) [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.fr.html)+.
Votre interface graphique devra être sous la même licence ou une compatible.
L'interface graphique devra pouvoir fonctionner sans [logiciel privateur](https://www.gnu.org/proprietary/).
Si vous ne savez pas ce qu'est [le logiciel libre](https://www.gnu.org/philosophy/free-sw.fr.html), Richard Stallman l'a expliqué à [PSES 2016](https://data.passageenseine.org/2016/webm/stallman.webm) (en français) et au [TEDx en 2014](https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society) (en anglais mais plus court).

Vous devrez versionner le projet avec git.
Le projet devra être disponible via Internet (par exemple via [GitLab.com](https://gitlab.com/), ou [NotABug.org](https://notabug.org/), mais [GitHub n'est pas une option](http://carlchenet.com/2016/01/22/le-danger-github/)).

Les sources sont disponibles (suivez les liens du premier paragraphe).
Elles compilent sous au moins Debian GNU/Linux 8 et Ubuntu 14.04.
N'hésitez pas à vous amuser avec (il y a les explications pour) et à poser des questions.

## Contact

- Client : Nicola Spanti, nicola.spanti@ecole.ensicaen.fr
