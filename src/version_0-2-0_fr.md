# PlanetWars2dRT 0.2.0, début d'un jeu libre 2D de bataille spatiale temps réel

En parcourant le [Firefox Marketplace](https://marketplace.firefox.com/) (le store avant tout pour [B2G](https://wiki.mozilla.org/B2G) / [Firefox OS](https://linuxfr.org/users/patrick_g/journaux/la-fin-de-firefox-os)), j'ai découvert [Auralux](http://www.auraluxgame.com/).
C'est un jeu de bataille spatiale temps réel en 2D, simple mais prenant.
Il y a [une version web](http://www.auraluxgame.com/game/) (qui ne marche plus à cause d'un TLS mal configuré) et des versions natives mais pas pour GNU/Linux. Beaucoup plus embêtant pour moi, c'est un logiciel privateur.

Des planètes qui produisent des vaisseaux, des vaisseaux et des planètes qui s'affrontent en se rentrant dedans, une interface graphique simple, bref ça ne parait pas trop compliqué. Mais il faut tout de même du temps.
Cette année scolaire là (2015-2016), je devais faire un projet sur toute l'année, aucun des sujets proposés ne m'emballait particulièrement.
Je n'avais qu'à proposer le mien, et espérer que ça intéresse au moins un autre camarade puisqu'un des buts étaient de nous faire travailler ensemble.
En voila le résultat, sous licences libres (surtout de l'[AGPL](https://www.gnu.org/licenses/agpl.fr.html)v3+ et de la [LGPL](https://www.gnu.org/licenses/lgpl.fr.html)v3+) et fonctionnant sous GNU/Linux !
D'une manière très original, le projet se nomme [PlanetWars2dRT](http://www.nicola-spanti.info/fr/projects/programming/apps/planet-wars-2d-rt.php) (RT étant pour Real-Time).

## Le cahier des charges

![La base du gameplay](src/images/simple-game-schema_fr.pdf)

Le but était de faire un jeu similaire à Auralux, sans pour autant le copier, juste sans servir comme base pour les concepts.
Le plus simple techniquement est de faire un jeu natif, surtout que mon école (l'ENSICAEN) apporte très peu d'importance au Web, ce qui aurait été contraignant pour les camarades qui m'ont rejoint et qui ne connaissaient presque rien au JavaScript.
Cependant, j'avais dans la tête de faire en sorte que je le jeu soit facilement portable sur le Web, avec un outil comme [emscripten](https://kripken.github.io/emscripten-site/).
Il devait aussi fonctionner sous GNU/Linux et être entièrement libre (y compris ses dépendances et les outils nécessaires pour le faire tourner et compiler).

Le C se convertit apparemment bien en JavaScript, c'est solide (pas de cassage de compatibilité, typage statique, etc) et mes camarades connaissent aussi, c'est donc le langage qui a été choisi.
C'est rustique (par exemple comparé à du C++ ou Java, notamment pour ce qui est de la librairie standard), mais le projet est de faible ampleur et ne nécessite pas de structures de données compliquées.

Il fallait ensuite choisir un toolkit graphique.
Qt allait être appris en cours dans la seconde partie de l'année, très puissant mais pas sur que ça soit facilement convertissable pour le Web.
[SDL](https://libsdl.org/)2 lui est en C et géré à coup sur par emscripten, de plus il est utilisé par de nombreux jeux, il a donc été choisi.

Si vous voulez en savoir plus sur le cahier des charges, il y a [dépôt git dédié pour les idées autour du projet](https://gitlab.com/RyDroid/PlanetWars2dRT-ideas) (majoritairement en LaTeX, et avec un peu de Markdown).

## Ok, mais il y a quoi qui a été fait ?

Le moteur du jeu marche. Il est composé en 2 grandes parties : le modèle (comme sont stockés les données, etc) et le contrôleur (les collisions, des intelligences artificielles, etc).
Il est dans un dépôt git dédié, séparé de la vue, nommé [core](https://gitlab.com/RyDroid/PlanetWars2dRT-core).

SDL2 n'a pas vraiment enthousiasmé mes camarades.
De toute façon, [la vue en SDL2](https://gitlab.com/RyDroid/PlanetWars2dRT-SDL2) était peu avancé, donc [une autre en Qt](https://gitlab.com/RyDroid/PlanetWars2dRT-Qt-widgets) a été lancé.
Il y a des boutons, de belles images pour les planètes, et d'autres trucs, mais ça avance lentement, et il manque le plus important : l'affichage d'une partie !
Au moins la version SDL2 affiche une partie, et il ne reste plus qu'une ou deux semaines avant la fin du projet (pour l'école), mais un joueur humain ne peut toujours pas déplacer ses vaisseaux !
Mes camarades sont moins dégourdis que moins en programmation (mais ça se comprend étant donné qu'ils n'ont pas fait d'études en informatique avant alors que j'ai fait un DUT informatique), et il faut faire vite, la vue en SDL2 est donc reprise pour y ajouter le contrôle des vaisseaux du joueur humain.
Ouf, "fini" pour l'école à temps.

![Dépôts git avec du code source et modèle MVC](src/images/repos_fr.pdf)

C'est un début, mais loin d'être "user-friendly" et pas beaucoup testé.
Je souhaite donc continuer pour en faire une version stable avec plein de tests avant d'ajouter de nouvelles fonctionnalités.
Beaucoup de tests (unitaires avec [check](https://libcheck.github.io/check/), via des outils en ligne de commande comme [cppcheck](http://cppcheck.sourceforge.net/) et [vera++](https://bitbucket.org/verateam/vera/wiki/Home), des options des compilateurs, etc) ont été ajoutés, et il y a eu beaucoup de refactorisation.
La version 0.2.0 est enfin là (presque 1 an après le gel des fonctionnalités utilisateurs, hormis des options en ligne de commande) et stable.

![L'interface rustique avec SDL2](src/images/screenshoot_sdl2_version-0-2.png)

## Comment tester ?

La partie core nécessite uniquement un compilateur C99 (comme [GCC](https://gcc.gnu.org/) ou [Clang](http://clang.llvm.org/)), une bibliothèque standard C99, [make](https://en.wikipedia.org/wiki/Make_%28software%29), [CMake](https://cmake.org/) et la bibliothèque check.
Si vous n'avez pas cette dernière, installer la (au moins [Trisquel GNU/Linux](https://trisquel.info/), [Debian](https://www.debian.org/) et Ubuntu la proposent dans leurs dépôts) ou compiler la statiquement avec `make static-libs`.
Un simple `make` exécutera CMake, `make tests` fera les nombreux tests (mais vous aurez besoin de logiciels non cités ici).
Les sources peuvent se récupérer avec git : `git clone https://gitlab.com/RyDroid/PlanetWars2dRT-core.git`.
Pour plus d'informations, il y a [le fichier BUILDING.md à la racine](https://gitlab.com/RyDroid/PlanetWars2dRT-core/blob/master/BUILDING.md).

La partie SDL2 nécessite en plus la bibliothèque SDL2 et 2 extensions (image et gfx), que vous pouvez probablement récupérer via le gestionnaire de paquets de votre distribution favorite (`apt-get install libsdl2-dev libsdl2-gfx-dev libsdl2-image-dev` sous au moins Debian et Trisquel) ou via `make static-libs`.
Bien entendu, il faut la partie core, si vous ne l'avez pas installé au niveau système, utilisez `make externals/core`.
Pour finir, un simple `make` suffit.
Le chemin du binaire est `./release/bin/planet-wars-2d-rt-sdl2-gui`.
Il y a des options pour la fenêtre (notamment sa taille) que je vous laisse découvrir avec `--help`.
Encore une fois, les sources sont disponibles via git : `git clone https://gitlab.com/RyDroid/PlanetWars2dRT-SDL2.git`. Pour plus d'informations, il y a [le fichier BUILDING.md à la racine](https://gitlab.com/RyDroid/PlanetWars2dRT-SDL2/blob/master/BUILDING.md).

Il ne devrait pas y avoir de problème sous GNU/Linux.
Ça devrait fonctionner sous macOS (mais le dernier test sur cet OS remonte à loin).
En l'état, ça ne compile probablement pas sous Windows, mais ça ne devrait pas être compliqué de faire le nécessaire au vu de ce qui est utilisé.

## Ce n'est pas fini

Il reste de nombreuses choses à faire.
Je n'aurais pas le temps avant au moins mars, et c'est mieux à plusieurs tout en allant plus vite, n'hésitez donc pas à vous joindre à cette odyssée spatiale libre.

La plus évidente est l'interface graphique, qui est bien trop rudimentaire.
Je prévois personnellement de continuer celle en SDL2.
Mais la partie core est une librairie indépendante qui fait fortement abstraction du fonctionnement interne du moteur du jeu, donc il devrait être facile de faire une interface avec GTK, Qt, wxWidgets ou que sais je si ça vous tente.

Il n'y a toujours pas de version web, mais ce n'est plus une priorité pour moi.
Si un tiers ne la fait pas, il est probable qu'elle n'existe jamais.

Il pourrait être intéressant de pouvoir créer, enregistrer et charger son propre univers.
Ça étendrait la "durée de vie" du jeu, et permettrait aux non programmeurs de contribuer indirectement.
J'envisage personnellement de [faire avec du XML](https://gitlab.com/RyDroid/PlanetWars2dRT-XML/) avec [libxml2 du projet GNOME](http://www.xmlsoft.org/).

Quelques intelligentes artificielles ont été faites.
Elles ne sont pas très malignes et surtout très bourrins.
Il y a probablement beaucoup de choses intéressantes à faire, et il y a la contrainte du temps réel.
J'ai peu d'idées sur le sujet, ça n'est pas à court terme ma priorité, mais avec un peu de temps il n'est pas dur d'en créer une au moins pour qu'il y en ait plus (pour diversifier les adversaires).
Si vous voulez en faire au moins une, vous pourriez être intéressés par les cartes statiques (c'est-à-dire codés en dur) qui permettent de comparer différentes IA et le générateur aléatoire qui vous offre une infinité de cas différents pour voir comment se comporte une IA.

J'aimerais qu'il soit packagé pour des distributions.
Pas juste qu'il soit possible de faire des paquets (c'est déjà fait automatiquement en deb et rpm via [CPack](https://cmake.org/Wiki/CMake:Packaging_With_CPack)), mais qu'ils soient dans les dépôts officiels.
Je voulais au moins le faire pour Debian, mais la complexité pour faire des paquets propres m'a découragé pour l'instant (malgré [le très bon tutoriel de Vincent Bernat](https://vincent.bernat.im/fr/blog/2016-empaquetage-debian-empirique.html), mais trop simple pour mon cas).
Au moins, si quelqu'un veut s'y coller, il y a de bonnes grosses bases ([hardening](https://wiki.debian.org/Hardening), fichiers nécessaires pour la partie core, etc), et les outils utilisés sont standards et déjà packagés.

Bon jeu (et contributions ?) !
