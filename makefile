# Copyright (C) 2015-2016  Nicola Spanti <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Commands
RM=rm -f
MKDIR=mkdir -p
LATEX2PDF=$(MKDIR) build && cd src && \
	pdflatex -output-format pdf -output-directory ../build
INKSCAPE=inkscape --without-gui
DIA=dia --nosplash

# Package and archive
PACKAGE=PlanetWars2dRT-ideas
SRC_FILES_TO_ARCHIVE=src/*.tex LICENSE_*.txt makefile *.md \
	.gitignore .editorconfig .gitlab-ci.yml


all: ideas kick-off mi-parcours gui version_0-2-0


open-ideas-fr: ideas-fr
	xdg-open build/ideas.fr.pdf &

open-kick-off-fr: kick-off-fr
	xdg-open build/kick-off.fr.pdf &

open-mi-parcours: mi-parcours
	xdg-open build/mi-parcours.pdf &

open-gui-text: gui-text
	xdg-open build/gui_text.fr.pdf &

open-gui-presentation: gui-presentation
	xdg-open build/gui_presentation.fr.pdf &

open-version-0-2-0: version_0-2-0
	xdg-open build/version_0-2-0_fr.pdf &


ideas: ideas-fr

ideas-fr: build/ideas.fr.pdf

build/ideas.fr.pdf: \
		src/ideas*.fr.tex \
		src/preamble.fr.tex src/preamble.tex \
		src/images/simple-game-schema_fr.pdf \
		src/*.xml src/*.dtd
	$(LATEX2PDF) ideas.fr.tex
	$(LATEX2PDF) ideas.fr.tex


kick-off: kick-off-fr

kick-off-fr: build/kick-off.fr.pdf

build/kick-off.fr.pdf: \
		src/kick-off*.fr.tex \
		src/preamble.fr.tex src/preamble_beamer.tex src/preamble.tex \
		src/images/simple-game-schema_fr.pdf src/images/real-time-problems_fr.pdf
	$(LATEX2PDF) kick-off.fr.tex
	$(LATEX2PDF) kick-off.fr.tex


mi-parcours: build/mi-parcours.pdf

build/mi-parcours.pdf: \
		src/mi-parcours.tex \
		src/preamble.fr.tex src/preamble_beamer.tex src/preamble.tex \
		src/images/repos_fr.pdf src/images/model.pdf
	$(LATEX2PDF) mi-parcours.tex
	$(LATEX2PDF) mi-parcours.tex


gui: gui-fr

gui-fr: gui-text-fr gui-presentation-fr

gui-text: gui-text-fr

gui-presentation: gui-presentation-fr

gui-text-fr: build/gui_text.fr.pdf

build/gui_text.fr.pdf: src/gui_text.fr.md
	pandoc src/gui_text.fr.md -o build/gui_text.fr.pdf

gui-presentation-fr: build/gui_presentation.fr.pdf

build/gui_presentation.fr.pdf: \
		src/gui_presentation.fr.tex \
		src/preamble.fr.tex src/preamble_beamer.tex src/preamble.tex \
		src/images/simple-game-schema_fr.pdf src/images/repos_fr.pdf
	$(LATEX2PDF) gui_presentation.fr.tex
	$(LATEX2PDF) gui_presentation.fr.tex


version_0-2-0: version_0-2-0_fr

version_0-2-0_fr: build/version_0-2-0_fr.pdf

build/version_0-2-0_fr.pdf: \
		src/version_0-2-0_fr.md \
		src/images/simple-game-schema_fr.pdf src/images/repos_fr.pdf
	pandoc src/version_0-2-0_fr.md -o build/version_0-2-0_fr.pdf


src/images/simple-game-schema_fr.pdf: src/images/simple-game-schema_fr.svg
	$(INKSCAPE) \
		--export-pdf=src/images/simple-game-schema_fr.pdf \
		src/images/simple-game-schema_fr.svg

src/images/real-time-problems_fr.pdf: src/images/real-time-problems_fr.svg
	$(INKSCAPE) \
		--export-pdf=src/images/real-time-problems_fr.pdf \
		src/images/real-time-problems_fr.svg

src/images/questions.pdf: src/images/questions.svg
	$(INKSCAPE) \
		--export-pdf=src/images/questions.pdf \
		src/images/questions.svg

src/images/repos_fr.pdf: src/images/repos_fr.svg
	$(INKSCAPE) \
		--export-pdf=src/images/repos_fr.pdf \
		src/images/repos_fr.svg

src/images/model.pdf: src/images/model.svg
	$(INKSCAPE) \
		--export-pdf=src/images/model.pdf \
		src/images/model.svg

src/images/model.svg: src/images/model.dia
	$(DIA) --filter=svg \
		--export=src/images/model.svg \
		src/images/model.dia


check-xml:
	xmllint src/*.xml --dtdvalid src/gamesave.dtd --noout


clean: clean-tmp clean-build
	cd src        && make --directory=..    clean-tmp  clean-latex
	cd src/images && make --directory=../.. clean-tmp  clean-latex

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* log/ logs/ \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-archives:
	$(RM) -f -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav *.out \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.bzip *.synctex.xz \
		*.synctex.lz *.synctex.lzma \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-build:
	$(RM) -rf -- build/ Build/ BUILD/

clean-git:
	git clean -fdx
